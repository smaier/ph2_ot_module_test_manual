\documentclass[
a4paper,
11pt,
]{article}
\usepackage{graphicx}
\usepackage{lmodern}
%\usepackage[utf8x]{fontenc}
\usepackage[utf8]{inputenc}
%\usepackage[german]{babel}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{amsbsy}
\usepackage[left=3.0cm,right=3.0cm,top=3cm,bottom=3cm]{geometry}
\usepackage{fancyhdr}
\usepackage[bf,small]{caption}
\usepackage{tikz}
\usepackage{ifthen}
\usepackage{xkeyval}
%\usepackage{xcolor}
\usepackage{calc}
\usepackage{hyperref}
\usepackage{todonotes}
%\usepackage{siunitx}
\usepackage[nohyperlinks]{acronym}
\usepackage{emptypage}
\usepackage[absolute,overlay]{textpos}
\usepackage{feynmf}
\usepackage{float}
\usepackage{floatflt}
\usepackage{placeins}
\usepackage{textcomp}
% Use more sophisticated cross references (\cref)
\usepackage[noabbrev]{cleveref}
% Subfigures
\usepackage{subcaption}
\usepackage{listings}
\usepackage{tabto}
\usepackage{siunitx}

\newcommand{\pt}{p$_\textrm{T}$ } 
\newcommand{\pts}{p$_\textrm{T}$} 
\newcommand{\vcth}{V$_{\textrm{Cth}}$ }
\newcommand{\vcths}{V$_{\textrm{Cth}}$}
\newcommand{\el}{e$^-$ }
\newcommand{\els}{e$^-$}
\newcommand{\nm}{two }
\newcommand{\dg}{$^\circ$ }
\newcommand{\dgs}{$^\circ$}
\newcommand{\iv}{$I(V)$ }
\newcommand{\ivs}{$I(V)$}
\newcommand{\cv}{$C(V)$ }
\newcommand{\cvs}{$C(V)$}
\newcommand{\sr}{$^{90}$Sr }
\newcommand{\srs}{$^{90}$Sr}
\newcommand{\phacf}{Ph2\_ACF }
\newcommand{\phacfs}{Ph2\_ACF}
\newcommand{\psp}{power\_supply }
\newcommand{\psps}{power\_supply}


\lstdefinestyle{cmdStyle}
{
    backgroundcolor=\color{white},
    basicstyle=\scriptsize\color{black}\ttfamily,
    language=bash
}

\newif\ifcites\citesfalse

\date{s.maier@kit.edu\\Karlsruhe\\ \today \\ \vspace{1cm} v4.0}
\author{Stefan Maier (\& Roland Koppenhöfer) }
\begin{document}
\title{MANUAL \\ OT Module Test Bench and Readout GUI (GIPHT)}
\maketitle

%\begin{figure}[h]
%    \centering
%    \includegraphics[width=0.9\textwidth]{figures/HGJv3_GP_Module_18.png}
%\end{figure}

\newpage
\tableofcontents

\newpage

\section{Introduction}
\label{sec:introduction}
The production of silicon detector modules for the Phase 2 Upgrade of the CMS Outer Tracker~(OT) is distributed all over the world.
To ensure equal testing conditions for the functional module test during and after module assembly a dedicated test station was developed at KIT and distributed to all assembly and integration sites.
This manual summarizes the details about the module test station, its features and how to use it with the readout GUI \textbf{G}raphical user \textbf{I}nterface for \textbf{PH}ase 2 \textbf{T}racker objects: \textbf{GIPHT}.

\section{The Outer Tracker Module Test Bench}
\label{sec:testBench}
The OT module test bench was developed and produced at KIT.
The test bench is shown in \Cref{fig:testBench}.
\begin{figure}
    \centering
    \includegraphics[width=0.9\textwidth]{figures/TestBench.jpg}
	\caption[The Outer Tracker module test bench]{The Outer Tracker module test bench}
	\label{fig:testBench}
\end{figure}
It is an aluminium-covered box which can house one OT module resting on its carrier.
Each box contains:
\begin{itemize}
\item An ESD-safe mounting structure for the modules on their carriers
\item A saftey HV relay, disconnecting the HV on the modules once the lid is opened
\item Several feed-throughs such as low and high voltage, USB, dry air and one for the optical fibre
\item A KIRA system (for some of the boxes) which allows an external signal injection for 2S modules
\item A PCB attached to an Arduino Micro, which monitors the temperature and humidity and controls the KIRA system
\end{itemize}
To read out an OT module in the test bench it is necessary to connect low and high voltage power supplies as well as one FC7.
In case of the station shown in \Cref{fig:testBench} the FC7 is mounted in a nanoCrate, the low voltage power supply is a RhodeSchwarz4040 and the high voltage power supply is a Keithley2410.
To perform KIRA based tests, the FC7 additionally needs to equipped with a DIO5 FMC card. 

\subsection{Setting Up}

The Outer Tracker Module Test Bench needs to be placed on an ESD safe mat or table to guarantee a safe testing of the OT modules. 
All external connections to the module are made via the patch panel on the back side of the box. 
Depending if the test bench contains a KIRA system or not, the patch panel at the box backside looks slightly different. 
Both variants are shown in \Cref{fig:PatchPanel}. 
The patch panels offer
\begin{itemize}
\item A USB-A connection to connect to a readout PC on which the Ph2\_ACF power supply package is running. The USB connection is used to communicate via serial protocol with the Arduino inside the box for air temperature and humidity monitoring and eventually KIRA control. 
\item A SHV connector (\textit{HV}) to supply the sensor high voltage to the modules. The high voltage line inside the box is interrupted by a magnetic switch when the box lid is opened. Only when fully closed, the switch is closed. Only the inner line of the SHV cable is connected to the HV cable inside the box, the cable and connector shield is floating!
\item A LEMO connector (\textit{TRIG}) which only needs to be connected for boxes with a KIRA system. A LEMO cable has to be connected to the second channel of the DIO5 FMC card on the FC7 as shown in \Cref{fig:FiberConnectionsSFPCage}.
\item Two banana connectors (\textit{MB} and \textit{MY}) to provide the low voltage potential and GND to the modules. The color scheme has been adapted to the cables shipped with the SEHs. In this case the yellow connection has to be connected to GND and the blue connection to \SI{10.5}{\V}. 
\item Two banana connectors (red and black) for the supply voltage of \SI{5}{\V} of the HV line relay and eventually all chips in the KIRA system. With opened lid the current should be \SI{0}{\A} for boxes without a KIRA system and between \SI{7}{\mA} and \SI{14}{\mA}. When the box is fully closed and the HV relay closes the current should increase by approximately \SI{30}{\mA}. Otherwise the HV line is still interrupted. This voltage should be supplied at any time to guarantee a proper operation of the box!
\item For boxes with KIRA systems there are additionally two banana connectors (red and black) to provide an additionally \SI{5}{\V} supply voltage for the KIRA LEDs. For boxes without KIRA system there are green banana connectors mounted which are not connected to anything inside the box (\textit{NC}). 
\item A cable connector to ground the aluminum box if needed. The default configuration is to leave the aluminum box floating. When placed on an ESD safe mat or table, the module mounting structure is connected via the box to the ESD GND and, thus, the box inside is secured from ESD events. 
\item A \SI{6}{\mm} tube connector to connect to a dried air supply. Additionally, the connector offers a manual stopcock to interrupt the dried air flux e.g.\ to avoid the noise when operating with opened lid. 
\item A pass-through for the optical fiber with corresponding flexible 3D printed flexible plug to surround the optical fiber and allow a light-tight pass-through. A picture sequence how to mount the 3D printed plug is shown in \Cref{fig:FiberPassThrough}. 
The LC fiber connectors are labeled 6 and 7. \Cref{fig:FiberConnectionsSFPCage} indicate the correct order of the two fibers inside the SFP+ cage. 
\end{itemize}

\begin{figure}
    \centering
    \begin{subfigure}[t]{.49\textwidth}
	    \includegraphics[width=\textwidth]{figures/PatchPanel-2SBox.png}
	    \caption{}
	\end{subfigure}
	\begin{subfigure}[t]{.49\textwidth}
	    \includegraphics[width=\textwidth]{figures/PatchPanel-PSBox.png}
	    \caption{}
	\end{subfigure}
	\begin{subfigure}[t]{\textwidth}
		\centering
		\includegraphics[width=.49\textwidth]{figures/cabling-sketch.pdf}
		\caption{}
	\end{subfigure}
	\caption[The OT Module Test Bench Patch Panels]{The Outer Tracker module test bench panels for boxes with a KIRA system (a) and boxes without KIRA (b). A sketch on how to connect the HV power supply and LV power supply connectors to the patch panel is shown in (c). }
	\label{fig:PatchPanel}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=.7\textwidth]{figures/FC7-OpticalCage-FiberNumbering.png}
	\caption[How to connect the LC fibers to the SFP+ cage]{The LC fiber connectors are labeled 6 and 7 and need to be connected to the SFP+ cage like marked in the picture. If a KIRA system is used the input for the external trigger must be connected to channel 2 of the DIO5 card.}
	\label{fig:FiberConnectionsSFPCage}
\end{figure}

A sketch on how to connect the individual power supply connectors via banana or SHV cables to the patch panel is shown in \Cref{fig:PatchPanel}c. 
The GND of HV and the GND \SI{10.5}{V} low voltage power supply channel have to be connected outside the box!
As at the time of sending out the test benches, the module prototypes were not yet using the final circuit board developed at Fermilab to perform the electrical module connections, a luster terminal (white box on the box front wall, see \Cref{fig:LusterTerminal}) is installed in the box.
For the current module prototypes, the corresponding connections can be screwed into the named connections (\textit{Y}: yellow, \textit{B}: blue, \textit{HV}: high voltage). 
As soon as the Fermilab circuit board is available the accompanying cable can be used to connect to the board. 
Therefore, the black cable needs to be screwed into the \textit{Y} slot of the luster terminal, the red cable without a white tape at the end into the \textit{B} slot and the red cable with a white tape at the end into the \textit{HV} slot (see \cref{fig:FermilabCable}). 

\begin{figure}
    \centering
    \begin{subfigure}[t]{.49\textwidth}
	    \includegraphics[width=\textwidth]{figures/IMG_4646.jpg}
	    \caption{}
	\end{subfigure}
	\begin{subfigure}[t]{.49\textwidth}
	    \includegraphics[width=\textwidth]{figures/IMG_4647.jpg}
	    \caption{}
	\end{subfigure}
    \begin{subfigure}[t]{.49\textwidth}
	    \includegraphics[width=\textwidth]{figures/IMG_4649.jpg}
	    \caption{}
	\end{subfigure}
	\begin{subfigure}[t]{.49\textwidth}
	    \includegraphics[width=\textwidth]{figures/IMG_4650.jpg}
	    \caption{}
	\end{subfigure}
	\begin{subfigure}[t]{.49\textwidth}
	    \includegraphics[width=\textwidth]{figures/IMG_4651.jpg}
	    \caption{}
	\end{subfigure}
	\begin{subfigure}[t]{.49\textwidth}
	    \includegraphics[width=\textwidth]{figures/IMG_4652.jpg}
	    \caption{}
	\end{subfigure}
	\caption[Fiber Pass-Through Installation]{Installation steps for the optical fiber in the pass-through. A uninstalled pass-through connection was used to clarify the procedure. }
	\label{fig:FiberPassThrough}
\end{figure}

\begin{figure}
    \centering
    \begin{subfigure}[t]{.49\textwidth}
	    \includegraphics[width=\textwidth]{figures/IMG_4644.jpg}
	    \caption{}
	    \label{fig:LusterTerminal}
	\end{subfigure}
	\begin{subfigure}[t]{.49\textwidth}
	    \includegraphics[width=\textwidth]{figures/Fermilab-Kabel.png}
	    \caption{}
	    \label{fig:FermilabCable}
	\end{subfigure}
	\caption[Luster Terminal and Fermilab Adapter Cable]{The luster terminal used in the module test bench to connect the module HV and LV cables (a). The cable to be connected to the Fermilab circuit board with the corresponding cables is shown in (b). }
\end{figure}

The box provides two manual valves to control the air flux directly into the box and to the outlets mounted in the box lid. 
The valve named \textit{module} controls the flux guided into the lid. 

To mount a module on its aluminum carrier in the box, the following steps have to be performed:
\begin{enumerate}
\item Bring the carrier slightly tilted in contact with the alignment pins as shown in \Cref{fig:ModuleInstallation}.  
\item Lower the carrier front side and push it onto the black holding plate. The lateral spring pushers will engage the carrier and hold it in position. For boxes with a KIRA system, the bottom KIRA system will align with the 2S module carrier or make room in case of a PS module carrier. 
\end{enumerate}


\begin{figure}
    \centering
    \begin{subfigure}[t]{.49\textwidth}
	    \includegraphics[width=\textwidth]{figures/IMG_4657.jpg}
	    \caption{}
	\end{subfigure}
	\begin{subfigure}[t]{.49\textwidth}
	    \includegraphics[width=\textwidth]{figures/IMG_4659.jpg}
	    \caption{}
	\end{subfigure}
	\caption[Module Installation]{Module carrier installation procedure: the carrier is first brought into contact with the alignment pins while being slightly tilted (a) and then lowered onto the PE plates (b). }
	\label{fig:ModuleInstallation}
\end{figure}

\subsection{Arduino Firmware Update}

The Arduino in the module test benches need to be flashed with a firmware able to read out the humidity and temperature sensor and control the KIRA system. 
GIPHT provides the functionality to flash the firmware to the most recent version.
See \ref{sssec:arduino}.

\subsection{Karlsruher InfraRed Array: KIRA }
\label{ssec:kira}
The \textbf{K}arlsruher \textbf{I}nfra\textbf{R}ed \textbf{A}rray \textbf{KIRA} serves as an external signal injector for 2S Modules. It consists of one motherboard and two daughterboards, shown in \Cref{fig:KiraBoards}. The motherboard is attached and controlled by an Arduino Micro. Both daughterboards are connected with a flat band cable to the motherboard. The KIRA system needs \SI{5}{\V} to operate, which is provided by the patch panel of the box. The mainboard provides the connection to the temperature and humidity sensor and generates the signal pulse to trigger the light emission on the IR LEDs mounted on the daughterboards. More details about the KIRA system can be found in~\cite{braach}.

\begin{figure}
    \centering
    \begin{subfigure}[t]{.49\textwidth}
	    \includegraphics[width=\textwidth]{figures/motherboard.jpg}
	    \caption{}
	    \label{fig:MotherBoard}
	\end{subfigure}
	\begin{subfigure}[t]{.49\textwidth}
	    \includegraphics[width=\textwidth]{figures/daughterboard.jpg}
	    \caption{}
	    \label{fig:DaughterBoard}
	\end{subfigure}
	\caption[Motherboard and daughterboard of KIRA system]{Motherboard (a) and daughter board (b) of the KIRA system. }
\label{fig:KiraBoards}
\end{figure}

\subsubsection{Motherboard}
\label{sssec:motherboard}
The KIRA motherboard is connected to and controlled by an Arduino Micro. A programmable nanosecond pulse generator triggers a light emission on the daughterboards. The trigger pulse is also forwarded to the FC7 to synchronize the module readout with the light injection. Both, the trigger frequency as well as the duration can be adjusted.

\subsubsection{Daughterboard}
\label{sssec:daughterboard}
On each daughterboard eight powerful IR LEDs are mounted. The illumination time is given by the trigger pulse length coming from the motherboard. The intensity of each LED can be adjusted separately with a 16 bit DAC setting. The intensity range can be switch to a high and low range. For standard application low range setting is sufficient.

\subsubsection{Software Control}
\label{sssec:software_control}
Once powered the software running on the arduino waits for commands at its serial port. It is possible to control the software with eleven commands:
\begin{itemize}
\item \textbf{version} Returns the current version of the software to check if the system runs with the most recent configuration.
\item \textbf{temperature} Returns the temperature read out at the attached temperature sensor.
\item \textbf{humidity} Returns the humidity read out at the attached humidity sensor.
\item \textbf{dewpoint} Returns the dewpoint calculated with the measured temperature and humidity.
\item \textbf{dacLed\_high} Set the DAC range of the LEDs to high.
\item \textbf{dacLed\_low} Set the DAC range of the LEDs to low.
\item \textbf{intensity\_XX\_YYYYY} Set the intensity of LED XX to intensity YYYYY (integer).
\item \textbf{pulseLength\_XXXXXX} Set the duration of the trigger pulse to XXXXXX in steps of 25 ns.
\item \textbf{frequency\_XXXXX} Set the frequency of the trigger pulse to XXXXX. With the default configuration the maximum trigger frequency is about 40 kHz.
\item \textbf{trigger\_on} Turn on the trigger output.
\item \textbf{trigger\_off} Turn off the trigger output.
\end{itemize}

\section{Graphical user Interface for PHase 2 Tracker objects: GIPHT}
\label{sec:gipht}
As described in \Cref{sec:testBench}, there are several devices that need to be controlled to test an OT module.
The communication with the power supplies and the Arduino in the test box is handled with a software package called \psp~ \cite{powerSupplyPackage}.
It can control all power supplies that are commonly used by the tracker upgrade group and supports connections via serial bus or Ethernet.
The communication with the FC7, and thus the module readout, is handled by the \phacf software package~\cite{ph2acf}.
GIPHT communicates with the \psp via TCP, the \phacf is controlled via a python wrapper serving the state machine of the \phacf tools~\cite{gipht}.

\subsection{Software Installation}
\label{ssec:softwareSetup}
The software installation is fairly easy because all additionally necessary software packages are included as submodules or can be installed with a requirements file.
To clone the gipht repository do
\begin{lstlisting}[style=cmdStyle, basicstyle=\tiny]
  $ git clone --recurse-submodules -b master https://gitlab.cern.ch/cms_tk_ph2/gipht.git
\end{lstlisting}
Afterwards enter the folder and install python3 and pip as well as the requirements
\begin{lstlisting}[style=cmdStyle, basicstyle=\tiny]
  $ cd gipht
  $ sudo dnf install python3
  $ sudo dnf install -y python-root
  $ sudo python3 -m pip install -U pip
  $ sudo python3 -m pip install -r requirements.txt
\end{lstlisting}
Afterwards run
\begin{lstlisting}[style=cmdStyle]
  $ sh ./compileSubModules.sh
\end{lstlisting}
to compile the C++-based submodules. Note, that a successful compiling of the \phacf and \psp package requires to install the listed packages as described in \cite{ph2acf} and \cite{powerSupplyPackage}.
To be able to open the module result files with a TBrowser it is advisable to have ROOT installed as described in the requirements for \phacf.
In addition to that GIPHT requires to install \textit{python-root} package to be able to format the result folders in a POTOTA-compatible format.
After the packages are installed and compiled, GIPHT is started by
\begin{lstlisting}[style=cmdStyle]
  $ python3 gipht.py
\end{lstlisting}
depending on how python 3 is configured on the machine (python, pyhton3.X, python3).

\subsection{Configuration of GIPHT}
\label{ssec:configureGipht}

\begin{figure}
    \centering
    \includegraphics[width=0.9\textwidth]{figures/gipht_configurations_marked.png}
	\caption[GIPHT configuration]{GIPHT configuration tab. Red indicates general settings for operation. Green shows all configured devices. Blue shows the individual connections settings of one devices while yellow shows the status of power supply channels.}
	\label{fig:configurationTab}
\end{figure}

Before GIPHT can be used to test OT modules some configurations must be made.
All settings are saved in a local file and are reloaded at the next start of GIPHT.
There are five tabs:
\begin{itemize}
\item Measurement: 	\tabto{2.7cm}Book modules and start measurements.
\item Results: 		\tabto{2.7cm}Recently generated result files.
\item Configuration:\tabto{2.7cm}General configuration of GIPHT and the devices connected.
\item Monitor: 		\tabto{2.7cm}Monitor plots of voltages, currents, temperature and humidity 
\item Expert:		\tabto{2.7cm}Expert tab, e.g. for DB connection or test settings

\end{itemize}
To configure GIPHT, choose the configuration tab as shown in \Cref{fig:configurationTab}.
Marked in red in \Cref{fig:configurationTab}, there are six folder and file settings in the top left area:
\begin{itemize}
\item \phacf folder: Should point to the previously cloned and compiled \phacfs.
\item Device package: Should point to the previously cloned and compiled \psp package.
\item Default 2S Module XML: This file defines the default 2S Module measurement XML for the \phacfs.
\item Default PS Module XML: This file defines the default PS Module measurement XML for the \phacfs.
\item Results Folder: Defines the location and name of the results folders. Each run has a separate folder named with an incremented number to store all generated result files.
\item Potato Folder: Defines the location where the resulting POTATO-root files are stored.
\end{itemize}
The button right next to the \phacf folder configuration executes gipht\_systemtes.cc in the \phacf to check whether the \phacf is compiled and ready to be used.
The button right next to it checks whether the Controlhub is running.
If this button is not green, the Controlhub is not started which can usually be done by:
\begin{lstlisting}[style=cmdStyle]
  $ cd /opt/cactus/bin
  $ ./start_controlhub
\end{lstlisting}
Below the folder and file configurations, there are seven check boxes:
\begin{itemize}
\item Show IV plot: During the IV measurement a live plot can be shown.
\item Invert IV plot: Invert the voltages and currents of the plot during an IV curve measurement.
\item IV VTRX+ Light OFF: Turn of the light of the VTRX+ when doing IV curves. This should always be enabled. Might be dropped in future versions.
\item Start Monitor: The monitor of the devices is under suspicion to slow down the performance of the GUI. Therefore, there is the possibility to turn off the monitor. This might also change in future versions.
\item Check devices on start-up: When starting GIPHT, all devices are read out once to see if they are responsive.
\item Power supply dialogues: Applying voltages and enabling power supply channels usually requires user confirmation. During a measurement run this can be skipped for convenience.
\item Check FW before Test: Based on the module ID given, check if the correct FW is loaded. It is not possible to query all details currently loaded on the FC7. Therefore, GIPHT must load the GUI at least once after start-up to know what is currently running on the FC7.
\end{itemize}

\subsection{Device Configuration}
\label{ssec:configureDevices}
The bottom left area of the configuration tab, indicated with the green box in \Cref{fig:configurationTab}, lists all devices connected to GIPHT with their configurations.
Devices can be added by pushing a button on the left.
Only the very last device can be removed.
While it is possible to add several Arduinos and power supplies, the number of FC7s is limited to one.
The connection settings are shown by clicking the \textit{Settings} button of the device.
As marked in blue in \Cref{fig:configurationTab} a dialogue shows up on the top right for the connection settings. First thing to determine is the type of connection which is either \textit{Serial} or \textit{Ethernet}.
The dialog should change according to the chosen connection, it is also different for an FC7.
\par Changing a device settings requires to restart the TCP severs by clicking \textit{Restart devices}.
After restarting, the devices are checked on their responsiveness by a simple readout command.
\par For each device the tree view shows the index, ID, type, connection as well as the status of the device, its server, monitor and connection.
The server status shows whether the TCP server of this device is running.
The servers are started by GIPHT in sub processes.
Their log files can be viewed in the folder \textit{log}.
For each device GIPHT launches a TCP server in a sub-process using the \psp package.
Then it creates a client sending the commands to this servers.
If the user wants to launch a server running on another machine the server creation can be suppressed by disabling the \textit{Launch Server} check box.
If disabled, the server must be entered with the IP:Port combination.
It is important that the server uses the same names of devices and channels to control the power supplies. 
By enabling the check box for the monitor, the corresponding device is polled every two seconds, except there already was a readout request within the last two seconds, e.g. by doing an IV curve.
The connection slider indicates the current connection status.
It can also be used to trigger a check of the device, which includes a readout try and a test of the TCP server.
The status field indicates the current task of the device.
\subsubsection{FC7}
\label{sssec:fc7}
In case of an FC7, GIPHT can search for available FC7s in the network by clicking \textit{Search FC7s}.
GIPHT pings all devices in the local network range and checks if the MAC addresses of the responding device does match with 08:00:30:00:*.
If yes, the IP address is displayed in the drop down menu.
It might be necessary to repeat the search two or three times, due to unreachable addresses and timeouts.
It is also possible to type in the FC7s IP manually.
The drop down menu on the very bottom shows all available firmwares on the FC7.
By choosing one the user can switch it.
The GIPHT repository also contains four firmwares that are compatible with the \phacf version that is used.
By clicking \textit{2S (FEC5)}, \textit{2S (FEC12)}, \textit{PS (5G)} or \textit{PS (10G)} the corresponding firmware is loaded on the FC7 or, if not already present, uploaded to the FC7 and then loaded.

\subsection{Arduino (Firmware Update)}
\label{sssec:arduino}
The temperature and humidity sensor inside the OT module test bench are read out with the provided Arduino placed next to the module fixture.
It is also used to control the two KIRA daughter boards, containing 8 powerful infra-red LEDs each.
The software to control the provided electronics is called \textit{KIRArduino}~\cite{arduinofirmware}
GIPHT contains this repository as sub-package.
By clicking \textit{Upload Arduino SW}, the software package is compiled and uploaded to the corresponding Arduino.
\textit{Configure LEDs and Trigger} opens a dialog with which it is possible to enable and adjust the LED intensities as well as the trigger frequency and pulse lengths of the light flashes.
Currently the software on the Arduino should have version 2.1.
If this is not the case, PS-flavoured test boxes (without KIRA) might have trouble reading out the humidity and temperature inside the test box.
Then the software must be updated as described above.



\subsubsection{Devices with Serial Connection}
\label{sssec:serial}
The serial connection takes quite some settings, which must be configured by the user according to the readout device.
The list of available serial ports can be updated by clicking \textit{Refresh} and afterwards the corresponding port can be chosen in the drop down menu.
Some devices have the possibility to change the connection settings as well.
Be sure that you always match the settings set or requested by the device and set in GIPHT.
Common problems with the serial connection are wrong settings, not properly working RS232/USB adapters or too long USB cables.
\subsubsection{Devices with Ethernet Connection}
\label{sssec:ethernet}
The Ethernet connection only requires an IP and a corresponding port. The default port is set depending on the chosen device type. This default settings are stored in the file \textit{settings/PowerSupplyModels.yml}.

\subsubsection{Power Supply Channels}
\label{sssec::channels}
Power supplies must have at least one power supply channel.
Otherwise the restart of the TCP servers is blocked.
The channels can be added and removed in the settings dialogue.
The channels are shown in the bottom right of the settings page, marked in yellow in \Cref{fig:configurationTab}
Each power supply has its own naming convention of channel names.
These names are shown as \textit{Channel} in the table and can be selected in a drop down menu.
It is not possible to add more channels than actually available by the corresponding power supply.
The information about which channels are available for a certain type of power supply is stored in a dictionary \textit{settings/PowerSupplyModels.yml} which must be extended for each power supply type used by the GUI.
The ID of a channel can be chosen by the user.
E.g. \textit{LowVoltageTestBox} to indicate that this channel is connected to a module in the OT test box.
The voltage to be applied is set in \textit{U\_set(V)}.
The voltage range for high voltage power supplies is $-801\,\textrm{V} < U < 0\,\textrm{V} $ and $ 0\,\textrm{V} > U > 11\,\textrm{V}$ for low voltage power supplies.
An input must be confirmed by pressing ENTER, and afterwards a message box pops up for an additional confirmation from the user.
For safety reasons, the voltage ramp function of the \psp does not allow to adjust voltages on channels that are not enabled, except the voltage is 0\,V.
\textit{U(V)} and \textit{I(V)} show the applied voltage and current on the channel.
The table also allows to enable or disable the channel by clicking on the red/green square at the very right.

\subsection{Monitor}
\label{ssec:monitor}
\begin{figure}
    \centering
    \includegraphics[width=0.9\textwidth]{figures/gipht_monitor.png}
	\caption[GIPHT Monitor]{Monitor tab of GIPHT. The page contains the monitor plots for the high voltage, low voltage and temperature and relative humidity.}
	\label{fig:monitorPage}
\end{figure}
The monitor page shows the recent readout values of the connected devices.
It is separated for low voltages/currents, high voltages/currents and temperature/humidity and dew point.
Each time a device is read out the result is appended to a data array stored in the device object, which is the plotted data.
These data arrays can be cleared with the button below the plots.

\subsection{Expert Tab}
\label{ssec:expert}
\begin{figure}
    \centering
    \includegraphics[width=0.9\textwidth]{figures/gipht_expert_tab.png}
	\caption[GIPHT Expert Tab]{Expert Tab of GIPHT. Used to adjust the DB settings and change test settings for the module test. Later can only be done enabling the \textit{Expert Mode}}
	\label{fig:expertTab}
\end{figure}
As shown in \Cref{fig:expertTab}, this tab contains settings about the database connection and the test routines
By default, you should choose the production DB and enable the two-factor-authentification.
By using a service account, disable two-factor-authentification.
GIPHT exploits the module \textit{py4dbupload} for the information request and data upload.
First time or, if expired, the user credentials must be entered in the terminal, which are then stored in a file named \textit{.session.cache}. 
For each start of GIPHT the user (except service accounts) must enter the one-time-password into the terminal for the two-factor-authentification.
The expert tab also shows the test settings for 2S Skeletons, 2S Modules and PS Modules. They are retrieved from the construction DB and should not be changed without reason. To do so, the \textit{Expert Mode} must be enabled.
During the next start-up of GIPHT these values will be overridden.

\subsection{Measurements}
\label{ssec:measurement}
\begin{figure}
    \centering
    \includegraphics[width=0.9\textwidth]{figures/gipht_measurements.png}
	\caption[GIPHT Measurements]{Measurement page. The operator, location connected devices and modules as well as all measurements are configured on this page. In addition to this it gives an overview on the current status of the measurements.}
	\label{fig:measurementPage}
\end{figure}
At the top right, the operator and location can be set.
If not already enlisted, an operator can be added by clicking \textit{Add}.
As every setting, it is automatically reloaded during the next start up of GIPHT.
The new name should be added to the list in the drop down menu right next to it.
If it is necessary to remove an operator edit \textit{settings/operator.yml}.
The location of the assembly site is also chosen via a drop down menu.
During the start-up of GIPHT, this list is updated from the construction DB.
The location list is saved in \textit{settings/locations.yml} and loaded if the construction DB is not accessible. 
\par GIPHT allows to test multiple modules in separate OT test boxes sequentially.
It is necessary to configure which power supply channel is connected to which box.
This information is stored in an object called \textit{Slot}.
The number of slots and thus readout boxes or modules can be changed by incrementing the number in the spin box at the very top of the measurement page.
A slot can be configured by \textit{Configure Slots}.
By default Slot 0 links to the Optical Group 0 of the FC7, and Slot 1 to Optical Group 1, and so on.
Each slot is displayed as one row on the right side of the measurement page.
Before a test is started the module ID is entered into the corresponding text field.
By entering the module ID GIPHT automatically retrieves information about the module from the construction DB.
If successful, the text field should turn green and the information can be viewed by \textit{Info}.
After each test the text field is emptied, to avoid forgetting to update the field while exchanging a module in the test box.
With the reverse Arrow button you can reload the previous module ID.
A checkbox in front of it activates or deactivates measurements on this slot.
At the end of each slot line the status about HV/LV voltage and current, as well as temperature and humidity that are connected to this particular slot.

\par On the very top right the measurements are configured.
GIPHT shows all possible measurements available by the \phacf for 2S and PS modules.
To ease the configuration, the two most important, The quick test and the full test can be chosen by a tick box.
The corresponding test is then chosen automatically in the drop down menu.
By default this also activates the IV curve measurement.

\par Each measurement is done with a sequence of tasks which are operated sequentially as threads.
All tasks are enqueued in a list which is shown on the bottom right area.
There are several measurement types:
The ID of the module is entered in the text field of the corresponding slot.
Ideally, this happens with a bar code reader, getting the ID directly from the label on the module or carrier to avoid typos.
By pressing ENTER, a search in the construction database is triggered.
If successful, the ID turns green and some general information about the module can be seen by clicking \textit{Info}.
Afterwards, the measurement can be started by clicking \textit{Start}.
GIPHT generates all necessary tasks and lists them in a tree view below the booked modules, and executes them one after another.
\textit{Stop Task} stops the current single task, \textit{STOP} stops all currently running and planned tasks.
After each measurement task, GIPHT checks for new result files in the corresponding folder and shows them on the results page.
\subsection{Results}
\label{ssec:results}
\begin{figure}
    \centering
    \includegraphics[width=0.9\textwidth]{figures/gipht_results.png}
	\caption[GIPHT Results]{Results of GIPHT.}
	\label{fig:results}
\end{figure}

As shown in \Cref{fig:results}, the generated results are displayed in a tree view on the results page.
Each run, e.g. an IV curve together with a noise measurement has its own results directory.
All files within this directory are shown in a tree format with their local run number.
In addition to the measured results files like the IV curve, there is a file summarizing the general data about operator, location etc, which is necessary for the later data upload.
The measurement files can be opened with a double-click.
For later module grading the results should be converted into the POTATO format, by click \textit{Format for POTATO}.
Doing so will also copy the resulting .root file in the set POTATO-folder.
This will load all relevant information into one single root file.
Before the data can be uploaded to the construction database the files need to be converted into an xml format with \textit{Convert for DB}.
Afterwards, the upload can be started with \textit{Upload to DB}.
All measurements that should be converted or uploaded must be activated by the check box at the first column of the entry. 
With \textit{Load result folders} previous measurements can be re-loaded into the GUI to have a look at them or to perform the upload if this was not possible for any reason.

\newpage
\vspace{5cm}
For any suggestions or comments please contact
\par Stefan Maier, s.maier@kit.edu


\begin{thebibliography}{widest entry}
\bibitem[1]{ph2acf} \url{https://gitlab.cern.ch/cms_tk_ph2/Ph2_ACF}
\bibitem[2]{powerSupplyPackage} \url{https://gitlab.cern.ch/cms_tk_ph2/power_supply}
\bibitem[3]{gipht} \url{https://gitlab.cern.ch/cms_tk_ph2/gipht}
\bibitem[4]{arduinoide} \url{https://www.arduino.cc/en/software}
\bibitem[5]{arduinofirmware} \url{https://gitlab.cern.ch/rkoppenh/KIRArduino.git}‌
\bibitem[6]{braach}\url{https://publish.etp.kit.edu/record/22024}

\end{thebibliography}

\end{document}
